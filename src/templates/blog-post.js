import React from "react";
import Link from "gatsby-link";
import Img from "gatsby-image";

export default ({ data }) => {
  const post = data.markdownRemark;
  return (
    <div>
      <Link to="/">
        Go to blog index
      </Link>
      <h1>{post.frontmatter.title}</h1>
      {post.fields.heroFile && <Img sizes={post.fields.heroFile.childImageSharp.sizes} />}

      <div dangerouslySetInnerHTML={{ __html: post.html }} />
    </div>
  );
};

export const query = graphql`
  query BlogPostQuery($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
      }
      fields {
        heroFile {
          childImageSharp {
            sizes(maxWidth: 700) {
              ...GatsbyImageSharpSizes_tracedSVG
            }
          }
        }
      }
    }
  }
`;
