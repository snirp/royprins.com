import React from "react";

import Card from "../components/page-link-card"
import Grid from "../components/grid"

export default ({ data }) => {
  const { edges: posts } = data.allMarkdownRemark;
  return (
    <div>
      <h1>My blog index</h1>
      <hr/>

      <Grid gutter="50" wrapMax="1200" rowCards="4">
        {data.allMarkdownRemark.edges.map(({ node }, index) =>
          <Card 
            key={index}
            title={node.frontmatter.title} 
            excerpt={node.excerpt} 
            imageFile={node.fields.heroFile}
            linkTo={node.fields.slug}
          />
        )}
      </Grid>

    </div>
  );
}

export const data = graphql`query PublishedPosts 
{ allMarkdownRemark(
    filter: { 
      frontmatter: {published: {eq: true}},
      fields: {source: {eq: "posts"}},
    }, 
    sort: {
      fields: [frontmatter___date], order: DESC
    }
  )
  {
    totalCount
    edges {
      node {
        id
        timeToRead
        wordCount {
          words
        }
        excerpt(pruneLength:500)
        frontmatter {
          title
          date
        }
        fields {
          slug
          heroFile {
            childImageSharp {
              sizes(maxWidth: 700) {
                ...GatsbyImageSharpSizes_tracedSVG
              }
            }
          }
        }
      }
    }
  }
}
`;
