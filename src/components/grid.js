import React from "react";
import styled, { css } from 'react-emotion';

// default props values
const defaults = {
  gutter: 10,
  wrapMax: 900,
  rowCards: 4,
}

const GridContainer = styled('div')`
  max-width: ${props => props.wrapMax || defaults.wrapMax}px;
  margin: 0 auto ${props => props.gutter || defaults.gutter}px;
`;

const GridInner = styled('div')`
  display: flex;
  flex-wrap: wrap;
  margin: 0 -${props => parseInt(props.gutter)/2 || defaults.gutter/2 }px;
`;

export default props =>
  <GridContainer wrapMax={props.wrapMax}>
    <GridInner gutter={props.gutter}>
      {React.Children.map(props.children, child =>
        React.cloneElement(child, {
          gutter: props.gutter || defaults.gutter,
          wrapMax: props.wrapMax || defaults.wrapMax,
          rowCards: props.rowCards || defaults.rowCards,
        })
      )}
    </GridInner>
  </GridContainer>
