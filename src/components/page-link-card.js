import React from "react";
import styled from 'react-emotion';
import Img from "gatsby-image";
import Link from "gatsby-link";

import ß from "../utils/scharf";


const Card = styled('div')`
  flex: 1 0 ${props => Math.floor( (parseInt(props.wrapMax)/(parseInt(props.rowCards)+1) ) )+1}px;
  margin: 0 ${props => props.gutter/2}px ${props => props.gutter}px;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

// Two cells only side-by-side if combined base > half container width
const Cardcell = styled('div')`
  flex: 1 0 ${props => Math.floor(parseInt(props.wrapMax)/4)+1}px;
  max-width: ${props => (parseInt(props.wrapMax)-parseInt(props.gutter))/2}px;
`;

const Col = styled('div')`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
`;

const Foot = styled('footer')`
  display: flex;
  background-color: white;
  justify-content: space-around;
  ${ß.py_3}
`

// TODO: set background image with cover
export default props =>
  <Card 
    gutter={props.gutter} 
    wrapMax={props.wrapMax} 
    rowCards={props.rowCards}
    >
    <Cardcell 
      gutter={props.gutter} 
      wrapMax={props.wrapMax}
      >
      {props.imageFile && <Img sizes={props.imageFile.childImageSharp.sizes} />}
    </Cardcell>
    <Cardcell 
      gutter={props.gutter} 
      wrapMax={props.wrapMax}
      >
      <Col>
        <h2><Link to={props.linkTo}>{props.title}</Link></h2>
        <p>{props.excerpt}</p>
        <Foot>
          <a href="">link</a>
          <a href="">link</a>
          <a href="">link</a>
        </Foot> 
      </Col>
    </Cardcell>
  </Card>