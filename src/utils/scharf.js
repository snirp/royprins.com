export default {
    bs_1: `box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);`,
    bs_2: `box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);`,
    bs_3: `box-shadow: 0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23);`,
    bs_4: `box-shadow: 0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22);`,
    bs_5: `box-shadow: 0 19px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22);`,
    px_1: `padding-left: 0.125rem; padding-right: 0.125rem;`,
    px_2: `padding-left: 0.25rem; padding-right: 0.25rem;`,
    px_3: `padding-left: 0.5rem; padding-right: 0.5rem;`,
    px_4: `padding-left: 1rem; padding-right: 1rem;`,
    px_5: `padding-left: 2rem; padding-right: 2rem;`,
    py_1: `padding-top: 0.125rem; padding-bottom: 0.125rem;`,
    py_2: `padding-top: 0.25rem; padding-bottom: 0.25rem;`,
    py_3: `padding-top: 0.5rem; padding-bottom: 0.5rem;`,
    py_4: `padding-top: 1rem; padding-bottom: 1rem;`,
    py_5: `padding-top: 2rem; padding-bottom: 2rem;`,
    bblr_1: `border-bottom-left-radius: 0.125rem;`
}
