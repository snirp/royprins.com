const { createFilePath } = require(`gatsby-source-filesystem`);
const path = require(`path`);

exports.onCreateNode = ({ node, getNode, boundActionCreators, getNodes }) => {
  const { createNodeField } = boundActionCreators;

  if (node.internal.type === `MarkdownRemark`) {
    const parentNode = getNode(node.parent)
    const source = parentNode.sourceInstanceName;
    if (source === `posts`) {
      const dirPart = path.parse(parentNode.absolutePath).dir.split("_")[1];

      // Provide string for regex constructor
      createNodeField({
        node,
        name: `heroFile`,
        value: `./hero.jpg`
      });
      createNodeField({
        node,
        name: `slug`,
        value: `/blog/${dirPart}/`
      });
      // Make filesystem source available to MarkdownRemark nodes
      createNodeField({
        node,
        name: `source`,
        value: source
      })
    }
  }
};



exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators;

  return new Promise((resolve, reject) => {
    graphql(`
      query PublishedBlogs {
        allMarkdownRemark(filter: {fields: {source: {eq: "posts"}}, frontmatter: {published: {eq: true}}}) {
          edges {
            node {
              fields {
                slug
              }
            }
          }
        }
      }
    `).then(result => {
      result.data.allMarkdownRemark.edges.forEach(({ node }) => {
        // Create blog posts
        createPage({
          path: node.fields.slug,
          component: path.resolve(`./src/templates/blog-post.js`),
          context: { 
            slug: node.fields.slug, 
          },
        });
      })
      resolve()
    })
  })
};

