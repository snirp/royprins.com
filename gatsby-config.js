module.exports = {

    plugins: [
      // You can have multiple instances of this plugin to read
      // source nodes from different locations on your filesystem.
      // {
      //   resolve: `gatsby-plugin-typography`,
      //   options: {
      //     pathToConfigModule: `src/utils/typography.js`,
      //   },
      // },
      `gatsby-plugin-emotion`,
      `gatsby-transformer-remark`,
      `gatsby-transformer-sharp`,
      `gatsby-plugin-sharp`,
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          path: `${__dirname}/src/data/posts`,
          name: `posts`,
        },
      },
      // {
      //   resolve: `gatsby-source-filesystem`,
      //   options: {
      //     path: `${__dirname}/src/images`,
      //     name: 'images',
      //   },
      // },

    ],
  };